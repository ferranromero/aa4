#include "Vector2D.h"
#include "Graph.h"




Graph::Graph(std::vector<std::vector<int>> terrain)
{
	this->terrain = terrain;

	for (int i = 0; i < terrain.size(); i++) {
		for (int j = 0; j < terrain[i].size(); j++) //recorremos el terrain 
		{

			if(validCell(Vector2D(j,i))) //comprobamos si la posici�n del terrain actual es una zona navegable y esta dentro del terrain
			{							 // si lo es, entra dentro del if
				Nodo* aux = nullptr;	//creamos un nodo auxiliar que sera nuestro "nodo actual, en el que estamos ahora"

				if (findNode(Vector2D(j, i)) == nullptr) //Si no existe, lo creamos
				{
					aux = new Nodo;
					aux->posicion = Vector2D(j, i);
					aux->peso = terrain[i][j];
					aux->padre = nullptr;
					aux->visitado = false;
					aux->heuristic = 0.f;

				}
				else {								//si existe, lo buscamos
					aux = findNode(Vector2D(j, i));
				}
			
				if (validCell(Vector2D(j-1, i))) { //Adyacente izquierdo es valido? Si lo es entra dentro del if
					
					Nodo* auxIzq = nullptr; //creamos el nodo izquierdo que no apunta a nada de momento

					if (findNode(Vector2D(j - 1, i)) == nullptr) //Si no existe lo creamos 
					{
						auxIzq = new Nodo;
						auxIzq->posicion = Vector2D(j - 1, i);
						auxIzq->peso = terrain[i][j - 1];
						auxIzq->padre = nullptr;
						auxIzq->visitado = false;
						auxIzq->heuristic = 0.f;

					}
					else {										//Si existe, lo buscamos en el mapa 
						auxIzq = findNode(Vector2D(j - 1, i));
					}
					
					aux->adyacentes.push_back(auxIzq);			//pusheamos el nodo auxiliar al vector de nodos adyacentes de nuestro "nodo actual"
					aux->numeroDeAdyacentes++;					//cada vez que hagamos un push al vector de adyacentes del "nodo actual" incrementamos la variable "numeroDeADyacentes", para saber el numero de nodos adyacentes que tiene el "nodo actual"
					map[auxIzq->posicion] = auxIzq; //Sirve para evitar volver a crear un nodo que ya se ha creado 
					auxIzq = nullptr;							//limpiamos el puntero (Buena praxis)
					
				}
				if (validCell(Vector2D(j + 1, i))) {//adyacente derecho es valido? Si lo es entra dentro del if

					Nodo* auxDer = nullptr;         //creamos el nodo izquierdo que no apunta a nada de momento

					if (findNode(Vector2D(j + 1, i)) == nullptr) //Si no existe lo creamos 
					{
						auxDer = new Nodo;
						auxDer->posicion = Vector2D(j + 1, i);
						auxDer->peso = terrain[i][j + 1];
						auxDer->padre = nullptr;
						auxDer->visitado = false;
						auxDer->heuristic = 0.f;
					}
					else {
						auxDer = findNode(Vector2D(j + 1, i));//Si existe, lo buscamos en el mapa 
					}

					aux->adyacentes.push_back(auxDer); //pusheamos el nodo auxiliar al vector de nodos adyacentes de nuestro "nodo actual"
					aux->numeroDeAdyacentes++;				//cada vez que hagamos un push al vector de adyacentes del "nodo actual" incrementamos la variable "numeroDeADyacentes", para saber el numero de nodos adyacentes que tiene el "nodo actual"
					map[auxDer->posicion] = auxDer;			 //Sirve para evitar volver a crear un nodo que ya se ha creado 
					auxDer = nullptr;						//limpiamos el puntero (Buena praxis)
				}
				if (validCell(Vector2D(j , i-1))) { //adyacente arriba

					Nodo* auxUp = nullptr;

					if (findNode(Vector2D(j, i-1)) == nullptr)
					{
						auxUp = new Nodo;
						auxUp->posicion = Vector2D(j, i - 1);
						auxUp->peso = terrain[i - 1][j];
						auxUp->padre = nullptr;
						auxUp->visitado = false;
						auxUp->heuristic = 0.f;
					}
					else {
						auxUp = findNode(Vector2D(j, i-1));
					}

					aux->adyacentes.push_back(auxUp);
					aux->numeroDeAdyacentes++;
					map[auxUp->posicion] = auxUp;
					auxUp = nullptr;
				}
				if (validCell(Vector2D(j, i + 1))) { //adyacente abajo 
					
					Nodo* auxDown = nullptr;

					if (findNode(Vector2D(j, i+1)) == nullptr)
					{
						auxDown = new Nodo;
						auxDown->posicion = Vector2D(j, i + 1);
						auxDown->peso = terrain[i + 1][j];
						auxDown->padre = nullptr;
						auxDown->visitado = false;
						auxDown->heuristic = 0.f;
					}
					else {
						auxDown = findNode(Vector2D(j, i+1));
					}
					
					
					aux->adyacentes.push_back(auxDown);
					aux->numeroDeAdyacentes++;
					map[auxDown->posicion] = auxDown;
					auxDown = nullptr;
				}
				map[Vector2D(j, i)] = aux; //a�adimos el "nodo actual" al mapa

				aux = nullptr;            //limpiamos el vector auxiliar(buena praxis)
			}
		}

	}

}



Graph::~Graph()
{
	for each (std::pair<Vector2D,Nodo*> nodo in map) //Recorremos el mapa y elmininamos los nodos. El "second" es el contenido de cada celda del mapa. 
	{
		delete nodo.second;
	}
}

std::unordered_map<Vector2D, Nodo*> Graph::getMap() //Funci�n que nos retorna el mapa, porque "mapa" es una variable privada de graph
{
	return map;
}




bool Graph::validCell(Vector2D position) { //Comprueva que la posici�n a la que queremos acceder existe y es un terreno navegable 
	if (position.x > 0 && position.x < terrain[position.y].size()) {
		if (position.y> 0 && position.y < terrain.size()) {
			return terrain[position.y][position.x] != 0;
		}
	}
	return false;
}

Nodo* Graph::findNode(Vector2D position) { //Nos encuentra un nodo dentro del mapa

	Nodo* aux = nullptr;
	if (map.find(position)!= map.end()) {
		aux = map[position];
	}
	return aux;
}

void Graph::restartMap() { //Resertea las variables del contenido de los nodos del mapa
	for each (std::pair<Vector2D, Nodo*> nodo in map)
	{
		nodo.second->visitado = false;
		nodo.second->padre = nullptr;
		nodo.second->heuristic = 0;
		nodo.second->socInici = false;
	}
}