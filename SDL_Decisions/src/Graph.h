#pragma once
#include "Vector2D.h"
#include <unordered_map>


namespace std              //hash table 
{
#define HASH(TYPE)																				\
	template<>																							\
	struct hash<TYPE>																				\
	{																											\
		size_t operator()(const TYPE & v) const											\
		{																										\
			size_t h = 2166136261;																\
			for (char * b = (char *)&v; b < ((char *)&v + sizeof(TYPE)); b++)	\
			{																									\
				h ^= *b;																					\
				h *= 16777619;																			\
			}																									\
			return h;																						\
		}																										\
	};

	HASH(Vector2D);

}



struct Nodo 
{
	Vector2D posicion;
	int peso=5;
	Nodo* padre;
	bool visitado;
	std::vector<Nodo*> adyacentes;
	int numeroDeAdyacentes=0;
	float heuristic;
	int cost_so_far=0;
	bool socInici = false;
};

class Graph
{
private:
	std::vector<std::vector<int>> terrain;
	std::unordered_map<Vector2D, Nodo*> map;
public:

	Graph(std::vector<std::vector<int>> terrain);
	~Graph();
	std::unordered_map<Vector2D, Nodo*> getMap();
	bool validCell(Vector2D position);
	Nodo* findNode(Vector2D position);
	void Graph::restartMap();
};

