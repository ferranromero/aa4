#include "PathFindingAlgorithms.h"
#include <queue>
#include <map>



PathFindingAlgorithms::PathFindingAlgorithms()
{
}

PathFindingAlgorithms::~PathFindingAlgorithms()
{
}

struct sobrecargaHeuristica
{
	bool operator() (const Nodo* n1, const Nodo* n2) const
	{
		return n1->heuristic > n2->heuristic;
	}
};

struct sobrecargaPeso
{
	bool operator() (const Nodo* n1, const Nodo* n2) const
	{
		return n1->cost_so_far > n2->cost_so_far;
	}
};

std::vector<Vector2D> PathFindingAlgorithms::BFS(Nodo * inici, Nodo * final)
{
	//Me cero un vector que es lo que retornar� la funcion 
	std::vector<Vector2D> camino;

	//sete a  no visitados todos los nodos del mapa 

	// Creamos la queue
	std::queue<Nodo*> frontier;

	//El primer nodo se marcar� como visitado y como no tiene padre su padre ser� igual a nullptr
	inici->visitado = true;
	inici->socInici = true;
	inici->padre = nullptr;

	//A�adimos el nodo inicio como primer ndoo en la queue
	frontier.push(inici);

	Nodo* current;

	while (!frontier.empty()) //recorre todos los adyacentes del nodo front de nuestra frontera
	{
		current = frontier.front();
		
		if (current->peso != 0)
		{
			for (int i = 0; i < frontier.front()->numeroDeAdyacentes; i++)
			{

				if (current->adyacentes[i]->visitado == false) { //Si el adyacente de el nodo front no ha sido visitado

					current->adyacentes[i]->visitado = true; //lo marcamos como visitado

					frontier.push(current->adyacentes[i]); //lo metemos dentro de nuestra frontera

					current->adyacentes[i]->padre = frontier.front(); //nos guardamos el padre del adyacente 


				}
				else if (current == final) { //si la posici�n del nodo de la frontera es igual al del nodo GOAL hacemos un break 
					break;
				}
			}
			frontier.pop(); //eliminamos el que esta en front para pasar a ver los adyacentes de los nodos ya guardados
		}
	}

	//Como no queremos que nuestro AGENT recorra todos los nodos que hemos recorrido solo cogeremnos el camino que nos de a partir del padre del del final
	//En este punto se supone que todos los nodos que hemos cogido tienen asignado un padre y apartir de este conseguiremos el camino deade el goal hasta el start

	Nodo*aux = final; //nuestro nodo current ser� igual al nuestro nodo GOAL
	int i = 0;

	do { // llenaremos el vector hasta que hayamos llegado al nodo START que no tiene padre
		camino.push_back(aux->posicion);
		aux = aux->padre;
	} while (aux->socInici != true); 

	std::reverse(camino.begin(), camino.end()); //Se tiene que hacer un revers al camino porque venimos de la posici�n del padre. 
	return camino;
}


float  PathFindingAlgorithms::ManhattanDistanceHeuristic(Vector2D n1, Vector2D n2)
{
	float dx = abs(n1.x - n2.x);
	float dy = abs(n1.y - n2.y);
	return (dx + dy);
}
