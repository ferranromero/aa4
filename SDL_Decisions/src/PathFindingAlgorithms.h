#pragma once
#include "Graph.h"
#include <list>
#include <queue>
#include "Path.h"

class PathFindingAlgorithms
{
public:
	PathFindingAlgorithms();
	~PathFindingAlgorithms();

	//Algorithms here!!

	std::vector<Vector2D>  BFS(Nodo* inici, Nodo* final);
	float ManhattanDistanceHeuristic(Vector2D n1, Vector2D n2);


};

